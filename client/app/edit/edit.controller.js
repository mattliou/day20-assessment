(function () {
    'use strict';
    angular
        .module("GL")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "GrocService","$stateParams"];

    function EditCtrl($filter, GrocService,$stateParams) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.name = "";
        vm.brand = "";
        vm.upc12 = "";
        vm.id = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        // vm.deleteManager = deleteManager;
        // vm.initDetails = initDetails;
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        // vm.updateDeptName = updateDeptName;
        vm.updateGrocValue = updateGrocValue;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();

        if ($stateParams.id) {
            vm.id=$stateParams.id;
            vm.search();
            
        }

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.name = "";
            vm.result.brand = "";
            vm.result.upc12 = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }

        //cancels edit information
        function cancelUpdate() {
            console.log("--show.controller.js > cancel()")
        }

        // Saves edited information
        function updateGrocValue() {
            console.log("-- show.controller.js > save()");
            GrocService
                .updateValue()
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        // Given a product id, this function searches the shop database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            //initDetails();
            vm.showDetails = true;

            GrocService
                .retrieveId(vm.id)
                .then(function (result) {
                    // Show table structure

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result = result.data
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        } 

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }
    }
})();