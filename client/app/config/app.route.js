(function(){
    var app = angular
        .module('GL')
        .config(uiRouteConfig);
    
    uiRouteConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function uiRouteConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: 'app/search/search.html'
            })
            .state('edit', {
                url: '/edit/:id',
                templateUrl: 'app/edit/edit.html'
            });
        $urlRouterProvider.otherwise('/search');
    }
})();
