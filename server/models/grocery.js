module.exports = function(sequelize, DataTypes) {
  return sequelize.define("grocery", {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
    },
    upc12: {
      type: DataTypes.BIGINT(12),
      allowNull: false, 
    },
    name: {
      type: DataTypes.CHAR(255),
    //   primaryKey: true,
      allowNull: false
    },
    brand: {
      type: DataTypes.STRING(255),
      unique: true,
      allowNull: false
    }
  }, {
    // don't add timestamps attributes updatedAt and createdAt
    timestamps: false,
    tableName: 'grocery_list',
  });
};