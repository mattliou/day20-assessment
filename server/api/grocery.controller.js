function retrieve(db) {
    return function (req, res) {
        db.Grocery
            .findAll({
                where: {
                    $or: [
                        { name: { $like: "%" + req.query.searchString + "%" } },
                        { brand: { $like: "%" + req.query.searchString + "%" } }
                    ]
                },
                limit: 20,
                order: [],
            })
            .then(function (grocery) {
                res
                    .status(200)
                    .json(grocery);
            })
            .catch(function (err) {
                console.log("groceryDB error clause: " + err);
                res
                    .status(500)
                    .json(err);
            });
    }
}

function retrieveId(db) { 
    return function(req,res) {
    
    db.Grocery
      .findOne({
        where: {
          id: req.params.id
        }
      })
      .then(function(grocery){
        res.json(grocery);
      })
      .catch(function(err){
        res.status(500).json({error:true});
      })
  }}

function updateGrocValue(db) {
    return function(req,res) {

    db.Grocery
      .update({
          params: {
              name: req.result.name,
              upc12: req.result.upc12,
              brand: req.result.brand,
          } 
      })
      .then(function(grocery){
          res.json(grocery);
      })
      .catch(function(err){
          res.status(500).json({error:true});
      })
    }
}


module.exports = function (db) {
    return {
        retrieve: retrieve(db),
        retrieveId: retrieveId(db),
    }
};
