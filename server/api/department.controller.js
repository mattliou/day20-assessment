// Handles department DB queries

// Retrieve from local memory
function retrieve(db) {
  return function(req, res) {
    var departments = [
      {
        deptNo: 1001,
        deptName: 'Admin'
      }, {
        deptNo: 1002,
        deptName: 'Finance'
      }, {
        deptNo: 1003,
        deptName: 'Sales'
      }, {
        deptNo: 1004,
        deptName: 'HR'
      }, {
        deptNo: 1005,
        deptName: 'Staff'
      }, {
        deptNo: 1006,
        deptName: 'Customer Care'
      }, {
        deptNo: 1007,
        deptName: 'Support'
      }
    ];

    res
      .status(200)
      .json(departments);
  }
};

// Retrieve from DB
function retrieveDB(db) {
  return function (req, res) {
    if (!req.query.searchString) req.query.searchString = '';
    
    db.products
      .findAll({
        where: {
          $or: [
            { product_name: { $like: "%" + req.query.searchString + "%" } },
            { product_no: { $like: "%" + req.query.searchString + "%" } }
          ]
        },
        limit: 20,
        order: [],
      })
      .then(function(products) {
        res
          .status(200)
          .json(products);
      })
      .catch(function(err) {
        console.log("productDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

// Retrieve department managers info
// var retrieveDeptManager = function(db) {
  return function(req, res) {
    if (!req.query.searchString) req.query.searchString = '';
    
    db.Department
      .findAll({
        where: {
          $or: [
            { dept_name: { $like: "%" + req.query.searchString + "%" } },
            { dept_no: { $like: "%" + req.query.searchString + "%" } }
          ]
        },
        include: [{
          model: db.DeptManager, 
          order: [["to_date", "DESC"]], 
          limit: 1, 
          include: [db.Employee]
        }],
      })
      .then(function (departments) {
        res
          .status(200)
          .json(departments);
      })
      .catch(function (err) {
        res
          .status(500)
          .json(err);
      });
  }
// };

// Create a new departmnet
// var create = function(db) {
  return function(req, res) {
    console.log('\nData Submitted');
    console.log('Dept No: ' + req.body.dept.id);
    console.log('Dept Name: ' + req.body.dept.name);

    db.Department
      .create({
        dept_no: req.body.dept.id,
        dept_name: req.body.dept.name
      })
      .then(function(department) {
        console.log(department.get({ plain: true }));
        res
          .status(200)
          .json(department);
      })
      .catch(function(err) {
        console.log("error: " + err);
        res
          .status(500)
          .json(err);
      });
  }
// };

// Export route handlers
module.exports = function(db) {
  return {
    retrieve: retrieve(db),
    // retrieveDB: retrieveDB(db),
    // retrieveDeptManager: retrieveDeptManager(db),
    // create: create(db),
  }
};
