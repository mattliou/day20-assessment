// Handles API routes

module.exports = function(app, db) {
  // var Department = require('./api/department.controller')(db);
  var Grocery = require('./api/grocery.controller')(db);
  app.get("/api/search", Grocery.retrieve);

  app.get("/api/grocery/:id", Grocery.retrieveId);




  // Create new department
  //app.post("/api/departments", Department.create);

  // Retrieve static department records
  //app.get("/api/departments", Department.retrieve);

  // Retrieve all department records that match query string passed. 
  //app.get("/api/departmentsDB", Department.retrieveDB);

  // Retrieve department records (including manager info) that match query string passed. 
  //app.get("/api/deptManagers", Department.retrieveDeptManager);
};
